import express from 'express';
import config from './config';
import path from 'path';
import { data } from './data';
const app = express();

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile('views/index.html', {root: path.dirname(__dirname)});
});

app.get('/data', (req, res) => {
    res.send(data);
});

app.listen(config.port, function listenHandler() {
    console.info(`Running on ${config.port}`);
});
