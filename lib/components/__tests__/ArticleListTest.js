import React from 'react';
import ArticleList from '../ArticleList';

import renderer from 'react-test-renderer';

const testProps = {
    articles: {
        a: { id: 'a', authorId: 'a' },
        b: { id: 'b', authorId: 'b' },
    },
    authors: {
        a: { id: 'a' },
        b: { id: 'b' },
    },
};

describe('ArticleListTest', () => {
    it('renders correctly', () => {
        const tree = renderer.create(<ArticleList {...testProps} />).toJSON();

        expect(tree.children.length).toBe(2);
        expect(tree).toMatchSnapshot();
    });
});
