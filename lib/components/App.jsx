import React from 'react';
import axios from 'axios';

import ArticleList from './ArticleList';

import DataApi from '../DataApi';
import { data } from '../data';

const api = new DataApi(data);

class App extends React.Component {
    state = {
        articles: api.getArticles(),
        authors: api.getAuthors(),
    };

    componentDidMount() {

    }

    render() {
        return (
            <ArticleList
                articles={this.state.articles}
                authors={this.state.authors}
            />
        );
    }
}

export default App;
