## Setup

[Install Yarn](https://yarnpkg.com/lang/en/docs/install)
yarn init
yarn add --dev eslint

[Install Eslint](https://eslint.org/docs/user-guide/getting-started#installation-and-usage)
yarn eslint -- --init
yarn add --dev eslint-plugin-react babel-eslint

[Install Express](https://expressjs.com/en/starter/installing.html)
yarn add express

[Add babel-cli](https://babeljs.io/docs/en/babel-cli)
yarn add @babel/core @babel/node @babel/cli babel-loader

[Add React and webpack](https://reactjs.org/)
yarn add react react-dom webpack webpack-cli

[Add babel extensions](https://babeljs.io/docs/)
yarn add @babel/preset-env @babel/preset-react @babel/polyfill yarn add @babel/plugin-proposal-class-properties
